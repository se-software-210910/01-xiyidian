package generator;

import com.javaclimb.drug.entity.Projects;

public interface ProjectsDao {
    int deleteByPrimaryKey(Integer projectId);

    int insert(Projects record);

    int insertSelective(Projects record);

    Projects selectByPrimaryKey(Integer projectId);

    int updateByPrimaryKeySelective(Projects record);

    int updateByPrimaryKeyWithBLOBs(Projects record);

    int updateByPrimaryKey(Projects record);
}