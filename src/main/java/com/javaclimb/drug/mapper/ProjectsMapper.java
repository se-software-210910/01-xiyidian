package com.javaclimb.drug.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaclimb.drug.entity.Projects;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProjectsMapper extends BaseMapper<Projects>{

}
