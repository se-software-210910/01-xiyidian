package com.javaclimb.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaclimb.drug.entity.Saleinfo;

/**
 * 记录的增删改查Mapper
 */
public interface SaleinfoMapper extends BaseMapper<Saleinfo> {
}
