package com.javaclimb.drug.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javaclimb.drug.entity.Problem;

/**
 * 问题的增删改查Mapper
 */
public interface ProblemMapper extends BaseMapper<Problem> {
}
