package com.javaclimb.drug.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.javaclimb.drug.entity.Owinfo;

import java.util.List;

/**
 * 出入库的service接口
 */
public interface IOwinfoService extends IService<Owinfo> {
    /**
     * 分页查询出入库数据
     * @param pageNum 第几页
     * @param pageSize 每页多少条数据
     * @param param 查询参数-出入库名称
     * @return
     */
    public IPage<Owinfo> selectOwinfoPage(int pageNum, int pageSize, String param);

    /**
     * 新增一条出入库信息
     * @param owinfo
     */
    public int addOwinfo(Owinfo owinfo);

    /**
     * 修改一条出入库信息
     * @param owinfo
     */
    public int editOwinfo(Owinfo owinfo);

    /**
     * 根据主键id查询一个出入库对象
     * @param id
     * @return
     */
    public Owinfo queryOwinfoById(Integer id);

    /**
     * 根据主键id删除一个出入库对象
     * @param id
     * @return
     */
    public int delOwinfoById(Integer id);

}
