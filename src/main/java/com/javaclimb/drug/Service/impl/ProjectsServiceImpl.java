package com.javaclimb.drug.Service.impl;

import com.javaclimb.drug.Service.ProjectsService;
import com.javaclimb.drug.entity.Projects;
import com.javaclimb.drug.mapper.ProjectsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectsServiceImpl implements ProjectsService {
    @Autowired
    private ProjectsService projectsService;
    @Autowired
    private ProjectsMapper projectsMapper;
    @Override
    public Projects getProjects(int id) {
        return null;
    }

    @Override
    public Projects insertInfo(Projects projects) {
        int i = projectsMapper.updateById(projects);
        return null;
    }

    @Override
    public int updateById(Projects projects) {
        return projectsMapper.updateById(projects);
    }

    @Override
    public List<Projects> selectAll() {
        return projectsService.selectAll();
    }
}
