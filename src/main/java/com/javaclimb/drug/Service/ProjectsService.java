package com.javaclimb.drug.Service;

import com.javaclimb.drug.entity.Projects;

import java.util.List;

public interface ProjectsService {
    Projects getProjects(int id);

    Projects insertInfo(Projects projects);


    int updateById(Projects projects);

    List<Projects> selectAll();
}
