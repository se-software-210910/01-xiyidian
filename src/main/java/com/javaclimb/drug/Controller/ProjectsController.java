package com.javaclimb.drug.Controller;

import com.javaclimb.drug.Service.ProjectsService;
import com.javaclimb.drug.entity.Projects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;
    @Controller
    @RequestMapping("/projects")
    public class ProjectsController {
        @Autowired(required = false)
        private ProjectsService projectsService;

        /**
         * 通过id查询项目信息
         *
         * @param id
         * @return
         */
        @RequestMapping("getInfo/{id}")
        public Projects getInfo(@PathVariable int id) {

            return projectsService.getProjects(id);
        }

        @RequestMapping("/insert/*")
        public Projects insert(Projects projects) {
            return projectsService.insertInfo(projects);
        }

        @RequestMapping("/update/*")
        public String updateById(Projects projects) {
            int result = projectsService.updateById(projects);
            if (result >= 1) {
                return "修改成功";
            } else {
                return "修改失败";
            }
        }

        @RequestMapping("/selectAll")
        public List<Projects> ListProjects() {
            return projectsService.selectAll();
        }

}
